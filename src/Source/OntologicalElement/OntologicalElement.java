/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Source.OntologicalElement;

import Source.Source;
import java.util.ArrayList;

/**
 *
 * @author murloc
 */
public class OntologicalElement {
    protected String uri;
    protected Source s;
    
    protected ArrayList<String> seeAlsos;
    
    public OntologicalElement(String uri, Source s){
        this.uri = uri;
        this.s = s;
        this.seeAlsos = new ArrayList<>();
    }
    
    public String getUri(){
        return this.uri;
    }
    
    public Source getSource(){
        return this.s;
    }
    
    public String toString(){
        return this.uri;
    }
    
    public ArrayList<String> getSeeAlsos(){
        return this.seeAlsos;
    }
    
}
